package gang;

import gang.conf.BookPropertyConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
//@EnableAutoConfiguration
@ServletComponentScan //使用该注解后，Servlet、Filter、Listener 可以直接通过 @WebServlet、@WebFilter、@WebListener 注解自动注册

/**一个组合的注解annotation is equivalent to using @Configuration, @EnableAutoConfiguration and @ComponentScan with their default attributes**/
@SpringBootApplication
public class WestApplication /*implements ApplicationContextAware*/{

	@Value("${book.name}")
	private String bookName;

	public String getBookName() {
		return this.bookName;
	}

	@Autowired
	private BookPropertyConf bookPropertyConf;

	@RequestMapping("/home")
    @ResponseBody
    String home() {
        return "Hello World 中国! " + this.getBookName() + " " + bookPropertyConf.getName();
    }

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(WestApplication.class, args);
//		WebMvcAutoConfiguration webMvcConfigurer = context.getBean(WebMvcAutoConfiguration.class);
//		System.out.println(webMvcConfigurer);

//		String[] beanNames = context.getBeanDefinitionNames();
//		for(String beanName : beanNames) {
//			System.out.println(beanName);
//		}
	}

//	@Override
//	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//		Map<String, HttpMessageConverter> maps = applicationContext.getBeansOfType(HttpMessageConverter.class);
////		for(Map.Entry<String, HttpMessageConverter> each : maps.entrySet()) {
////			if(each.getValue() instanceof StringHttpMessageConverter) {
////
////			}
////		}
//		//java8的写法
//		maps.forEach((key,value) -> {
//			if(value instanceof StringHttpMessageConverter) {
//				StringHttpMessageConverter stringHttpMessageConverter = (StringHttpMessageConverter)value;
//				stringHttpMessageConverter.setDefaultCharset(Charset.forName("UTF-8"));
//			}
//		});
//	}

	/**
     * 修改DispatcherServlet默认配置
     *
     */
//    @Bean
//    public ServletRegistrationBean dispatcherRegistration(DispatcherServlet dispatcherServlet) {
//        ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet);
//        registration.getUrlMappings().clear();
//        registration.addUrlMappings("*.do");
//        registration.addUrlMappings("*.json");
//        return registration;
//    }

    /**
     * 使用代码注册Servlet（不需要@ServletComponentScan注解）
     */
//    @Bean
//    public ServletRegistrationBean servletRegistrationBean() {
//        return new ServletRegistrationBean(new MyServlet(), "/xs/*");// ServletName默认值为首字母小写，即myServlet
//    }



}

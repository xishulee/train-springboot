/**
 * 
 */
package gang.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.async.DeferredResult;

import gang.service.PushService;

/**
 * @author xuxigang
 *
 */
@Controller
@RequestMapping()
public class AsyncController {
	
	@Autowired
	private PushService pushService;
	
	@GetMapping("/push")
	@ResponseBody
	public DeferredResult<String> async() {
		return pushService.getAsyncUpadate();
	}

}

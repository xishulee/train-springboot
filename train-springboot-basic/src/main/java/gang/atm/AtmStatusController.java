/**
 * 
 */
package gang.atm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import gang.domain.User;

/**
 * @author xuxigang
 *
 */
@Controller
@RequestMapping()
public class AtmStatusController {
	
	@GetMapping("/status")
	@ResponseBody
	public String atm() {
		return "我是中国人，，伟大的";
	}
	
	@GetMapping("/props")
	@ResponseBody
	public String props() {
		return "我的那个属相啊";
	}
	
	@GetMapping("/users")
	@ResponseBody
	public List<User> users() {
		List<User> users = new ArrayList<User>();
		User user = new User();
		user.setId(1);
		user.setUserCode("xuxigang");
		user.setUserName("徐西刚");
		users.add(user);
		return users;
	}
	
	@GetMapping("/user/1")
	@ResponseBody
	public User user() {
		User user = new User();
		user.setId(1);
		user.setUserCode("xuxigang");
		user.setUserName("徐西刚");
		return user;
	}
	
	@GetMapping("/maps")
	@ResponseBody
	public Map<String,Object> maps() {
		Map<String,Object> maps = new HashMap<String,Object>();
		maps.put("xuxigang", "徐西刚");
		User user = new User();
		user.setId(1);
		user.setUserCode("xuxigang");
		user.setUserName("徐西刚");
		maps.put("details",user );
		return maps;
	}


}

/**
 * 
 */
package gang.conf;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 
 * 异步的servlet
 * @author xuxigang
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan("gang.async")
public class AsyncDispatchServlet {
	@Bean
	public ServletRegistrationBean dispatcherRegistration(DispatcherServlet dispatcherServlet) {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet);
		registration.addUrlMappings("/async/*");
		registration.setName("async");
		registration.setLoadOnStartup(1);
		registration.setAsyncSupported(true);
		return registration;
	}
}

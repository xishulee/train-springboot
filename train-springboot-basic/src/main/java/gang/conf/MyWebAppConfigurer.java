/**
 * 
 */
package gang.conf;

import java.nio.charset.Charset;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import gang.interceptor.MyInterceptor;

/**
 * @author xuxigang
 *
 */
@Configuration
@EnableScheduling
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 多个拦截器组成一个拦截器链
		// addPathPatterns 用于添加拦截规则
		// excludePathPatterns 用户排除拦截
		registry.addInterceptor(new MyInterceptor()).addPathPatterns("/**");
		super.addInterceptors(registry);
	}

	@Bean
	public HttpMessageConverter<String> httpMessageResponseBodyConverter() {
		StringHttpMessageConverter converter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
		return converter;
	}

	/**
	 * 返回值为string值的中文乱码问题，如果加上这个，则导致actuator组件找不到url
	 * 还是总是返回的内容未json吧
	 */
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		super.configureMessageConverters(converters);
		converters.add(httpMessageResponseBodyConverter());
		for(HttpMessageConverter<?> converter :converters) {
			if(converter instanceof StringHttpMessageConverter) {
				StringHttpMessageConverter stringHttpMessageConverter = (StringHttpMessageConverter)converter;
				stringHttpMessageConverter.setDefaultCharset(Charset.forName("UTF-8"));
			}
		}
	}

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(false);//不知道啥意思
	}
}

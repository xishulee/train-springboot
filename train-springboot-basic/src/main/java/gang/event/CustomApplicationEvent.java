package gang.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created by xuxigang on 2018/7/23.
 */
public class CustomApplicationEvent  extends ApplicationEvent{

    public CustomApplicationEvent(Object source) {
        super(source);
    }
}

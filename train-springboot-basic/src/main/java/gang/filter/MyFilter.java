/**
 * 
 */
package gang.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * @author xuxigang
 *
 */
@WebFilter(value = "/*")
public class MyFilter implements Filter {

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("in filter...");
		chain.doFilter(req, resp);
		System.out.println("out filter...");
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

}

package gang.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author xuxigang
 *
 */

public class MyInterceptor implements HandlerInterceptor{
	  @Override
	  public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
	    System.out.println(MyInterceptor.class.getName()+" : 在请求之前调用");
	    return true;
	  }
	  @Override
	  public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
	    System.out.println(MyInterceptor.class.getName()+" :请求处理之后视图渲染之前使用");
	  }
	  @Override
	  public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
	    System.out.println(MyInterceptor.class.getName()+" :请视图渲染之后使用");
	  }
	}

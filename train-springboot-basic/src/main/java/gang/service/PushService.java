/**
 * 
 */
package gang.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * @author xuxigang
 *
 */
@Service
public class PushService {
	private DeferredResult<String> result;

	public DeferredResult<String> getAsyncUpadate() {
		result = new DeferredResult<String>();
		return result;
	}

	@Scheduled(fixedDelay = 5000000)
	public void refresh() {
		if (result != null) {
			result.setResult(Long.valueOf(System.currentTimeMillis()).toString());
		}else {
			System.out.println("5000");
		}
	}
}

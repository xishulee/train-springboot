package li.annotation;

import java.io.Serializable;

/**
 * @ClassName TokenInfo
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/7/18
 * @Version
 **/
public class TokenInfo implements Serializable {
    private boolean success;
    private String error;
    private UserInfo data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public UserInfo getData() {
        return data;
    }

    public void setData(UserInfo data) {
        this.data = data;
    }
}

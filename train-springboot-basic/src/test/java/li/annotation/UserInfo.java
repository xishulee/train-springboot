package li.annotation;

import java.io.Serializable;

/**
 * @ClassName UserInfo
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/7/18
 * @Version
 **/
public class UserInfo implements Serializable {
    private String userId;
    private String userName;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}

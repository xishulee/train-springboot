package xishu.core.form;

/**
 * Created by xuxigang on 2018/8/7.
 */
public class DropDownVo {
    private long id;
    private String text;
    private String value;

    public DropDownVo(){
        //default constructor
    }

    public DropDownVo(long id,String text,String value){
        this.id = id;
        this.text = text;
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

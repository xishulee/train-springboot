package xishu.core.form;

import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * 返回到页面结果集合
 * Created by xuxigang on 2018/7/25.
 */
public class HttpJsonResult<T> implements Serializable {
    private boolean success;
    private String errorMsg;
    private long total;
    private List<T> data;

    public HttpJsonResult() {
    }

    public HttpJsonResult(Page<T> page) {
        this.success = true;
        this.total = page.getTotalElements();
        this.data = page.getContent();
    }

    public HttpJsonResult(String errorMsg) {
        this.success = false;
        this.errorMsg = errorMsg;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}

package xishu.core.jpa;

import com.querydsl.jpa.HQLTemplates;
import com.querydsl.jpa.impl.JPAQuery;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;
import org.hibernate.type.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QuerydslJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/7/25.
 */
public class BaseRepository<T, ID extends Serializable> extends QuerydslJpaRepository<T, ID> implements IBaseRepository<T, ID> {
    //实体管理类，对持久化实体做增删改查，自动义sq操作模板所需要的核心类
    public final EntityManager entityManager;

    public BaseRepository(JpaEntityInformation<T, ID> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map> findByParams(String jql, Object... params) {
        Query query = entityManager.createQuery(jql).unwrap(Query.class);
        //查询结果转map
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        int i = 0;
        for (Object param : params) {
            query.setParameter(i++, param);
        }
        return query.list();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Map> findPageByParams(String sql, Pageable pageable, Object... params) {
        Session session = (Session) entityManager.getDelegate();
        Query query = session.createQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        int i = 0;
        for (Object param : params) {
            query.setParameter(i++, param);
        }

        List<Map> totalCount = query.list();
        query.setFirstResult(pageable.getPageSize() * (pageable.getPageNumber() - 1));
        query.setMaxResults(pageable.getPageSize());
        List<Map> pageCount = query.list();
        return new PageImpl<>(pageCount, pageable, totalCount.size());
    }

    /**
     *
     1
     Query query = entityManager.createNativeQuery("select id, name, age from t_user", User.Class);
     * @param sql
     * @param clazz
     * @param retColumnTypeMap 返回的列类型
     * @param <R>
     * @return
     */
    @Override
    public <R> List<R> findBySql(String sql, Class<R> clazz, Map<String,Type> retColumnTypeMap) {
        NativeQuery query = entityManager.createNativeQuery(sql).unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(clazz));
        retColumnTypeMap.forEach((String key,Type type) ->{
            query.addScalar(key,type);
        });
        return query.getResultList();
    }

    @Override
    public NativeQuery getNativeQuery(String sql){
        return entityManager.createNativeQuery(sql).unwrap(NativeQueryImpl.class);
    }

    @Override
    public NativeQuery getNativeQuery(String sql, Class clazz){
//        return entityManager.createNativeQuery(sql,clazz).unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(clazz));
        NativeQuery query =  entityManager.createNativeQuery(sql).unwrap(NativeQuery.class);
        query.setResultTransformer(Transformers.aliasToBean(clazz));
        return query;
    }

    @Override
    public JPAQuery getJpaQuery(){
        return new JPAQuery(entityManager, HQLTemplates.DEFAULT);
    }
}

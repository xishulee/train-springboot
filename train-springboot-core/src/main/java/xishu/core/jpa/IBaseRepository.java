package xishu.core.jpa;

import com.querydsl.jpa.impl.JPAQuery;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author xuxigang
 * Created by xuxigang on 2018/7/25.
 */

@NoRepositoryBean //该注解表示 spring 容器不会创建该对象
public interface IBaseRepository<T, ID extends Serializable> extends QuerydslPredicateExecutor<T>,JpaRepository<T,ID> {
    /**
     * sql查询
     *
     * @param sql
     * @param params
     * @return
     */
    List<Map> findByParams(String sql, Object... params);

    /**
     * sql分页查询
     *
     * @param sql
     * @param params
     * @return
     */
    Page<Map> findPageByParams(String sql, Pageable pageable, Object... params);

    /**
     * 分页查找
     * @param r
     * @param pageable
     * @return
     */
    Page<T> findAll(Specification<T> r, Pageable pageable);

   /**
     * 根据sql查找，并返回一个新类
     * @param sql
     * @param clazz
    *  @param retColumnTypeMap 返回的列类型
     * @return
     */
    <R> List<R> findBySql(String sql, Class<R> clazz, Map<String,Type> retColumnTypeMap);

    /**
     * 获取NativeQuery
     * @param sql
     * @return
     */
    NativeQuery getNativeQuery(String sql);

    /**
     * 获取nativeQuery
     * @param sql
     * @param clazz
     * @return
     */
    NativeQuery getNativeQuery(String sql,Class clazz);

    /**
     * 获取Dsl模式的JPAQuery
     * @return JPAQuery
     */
    JPAQuery getJpaQuery();

}

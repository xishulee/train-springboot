package xishu.core.query;

import xishu.core.jpa.IQuery;

import java.io.Serializable;

/**
 * 基础的查询条件
 * Created by xuxigang on 2018/7/25.
 */
public abstract class BaseQuery implements Serializable, IQuery {
    /**
     * 主键ID，
     */
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}

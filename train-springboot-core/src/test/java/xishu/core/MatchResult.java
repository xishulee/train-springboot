package xishu.core;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.alibaba.excel.annotation.write.style.HeadStyle;
import com.alibaba.excel.enums.BooleanEnum;
import com.alibaba.excel.enums.poi.BorderStyleEnum;
import org.apache.poi.ss.usermodel.IndexedColors;

/**
 * @ClassName MathResult
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/24
 * @Version
 **/
@ContentRowHeight(20)
@HeadRowHeight(20)
@ColumnWidth(25)
@HeadStyle(locked = BooleanEnum.TRUE, fillBackgroundColor = 14, borderBottom = BorderStyleEnum.DOTTED)
public class MatchResult {
    @ExcelProperty("服务名称(中文)")
    private String zhName;
    @ExcelProperty("服务名称(英文)")
    private String enName;
    @ColumnWidth(40)
    @ExcelProperty("配置文件")
    private String ymlFileName;
    @ExcelProperty("存储类型")
    private String fileType;
    @ExcelProperty("负责人")
    private String person;

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }


    public String getYmlFileName() {
        return ymlFileName;
    }

    public void setYmlFileName(String ymlFileName) {
        this.ymlFileName = ymlFileName;
    }


    public String getZhName() {
        return zhName;
    }

    public void setZhName(String zhName) {
        this.zhName = zhName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}

package xishu.core;

/**
 * @ClassName Project
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/24
 * @Version
 **/
public class Project {
    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getBizApp() {
        return bizApp;
    }

    public void setBizApp(String bizApp) {
        this.bizApp = bizApp;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    private String person;
    private String bizApp;
    private String appName;
    private String zhName;
    private String enName;

    public String getZhName() {
        return zhName;
    }

    public void setZhName(String zhName) {
        this.zhName = zhName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }
}

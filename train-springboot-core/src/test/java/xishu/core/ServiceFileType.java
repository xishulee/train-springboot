package xishu.core;

/**
 * @ClassName ServiceFileType
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/24
 * @Version
 **/
public class ServiceFileType {
    private String ymlFileName;
    private String fileType;
    public String getYmlFileName() {
        return ymlFileName;
    }

    public void setYmlFileName(String ymlFileName) {
        this.ymlFileName = ymlFileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}

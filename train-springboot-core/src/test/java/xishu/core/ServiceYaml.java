package xishu.core;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;

/**
 * @ClassName ServiceYaml
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/25
 * @Version
 **/
public class ServiceYaml {

    public String getYamlName() {
        return yamlName;
    }

    public void setYamlName(String yamlName) {
        this.yamlName = yamlName;
    }

    @ExcelProperty("服务名(英文)")
    @ColumnWidth(40)
    private String yamlName;

    public ServiceYaml(String yamlName) {
        this.yamlName = yamlName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServiceYaml that = (ServiceYaml) o;

        return yamlName.equals(that.yamlName);
    }

    @Override
    public int hashCode() {
        return yamlName.hashCode();
    }
}

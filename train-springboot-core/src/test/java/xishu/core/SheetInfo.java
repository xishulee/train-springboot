package xishu.core;

/**
 * @ClassName SheetInfo
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/25
 * @Version
 **/
public class SheetInfo {
    private int sheetNum;
    private String sheetName;

    public int getSheetNum() {
        return sheetNum;
    }

    public void setSheetNum(int sheetNum) {
        this.sheetNum = sheetNum;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public SheetInfo(int sheetNum, String sheetName) {
        this.sheetName = sheetName;
        this.sheetNum = sheetNum;
    }
}

package xishu.core;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.util.DateUtils;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName Xls
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/24
 * @Version
 **/
public class Xls {
    private static String projectPath = YamlTest.ROOT_PATH + "\\PMS3.0统推版本服务清单.xlsx";
    private static String fujianPath = YamlTest.ROOT_PATH + "\\手动整理.et";

    public static void main(String[] args) {
        Xls xls = new Xls();
        xls.change();
    }

    private void change() {
        List<Project> projects = EasyExcel.read(Xls.projectPath).head(Project.class).sheet().doReadSync();
        Map<String, Project> projectMaps = projects.stream().filter(s -> !StringUtils.isEmpty(s.getPerson()))
                .collect(Collectors.toMap(Project::getEnName, Function.identity()));

        ExcelReaderBuilder excelReaderBuilder = EasyExcel.read(Xls.fujianPath);
        ExcelReader excelReader = excelReaderBuilder.build();
        List<ReadSheet> sheets = excelReader.excelExecutor().sheetList();

        List<List<MatchResult>> results = new ArrayList<>();
        for (ReadSheet sheet : sheets) {
            System.out.println(sheet.getSheetNo() + " " + sheet.getSheetName());
            List<ServiceFileType> sheetInstance = excelReaderBuilder.head(ServiceFileType.class).sheet(sheet.getSheetNo()).doReadSync();

            List<MatchResult> matchResults = new ArrayList<>();
            for (ServiceFileType serviceFileType : sheetInstance) {
                MatchResult matchResult = new MatchResult();
                matchResult.setYmlFileName(serviceFileType.getYmlFileName());
                matchResult.setFileType(serviceFileType.getFileType());
                int length = serviceFileType.getYmlFileName().length();
                Project orDefault = projectMaps.getOrDefault(serviceFileType.getYmlFileName().substring(0, length - 4), null);
                if (orDefault != null) {
                    matchResult.setPerson(orDefault.getPerson());
                    matchResult.setZhName(orDefault.getZhName());
                    matchResult.setEnName(orDefault.getEnName());
                }
                matchResults.add(matchResult);
            }

            results.add(matchResults);
        }

        String resultFileName = YamlTest.ROOT_PATH + "\\result" + System.currentTimeMillis() + ".xlsx";
        ExcelWriter excelWriter = EasyExcel.write(resultFileName, MatchResult.class).build();
        WriteSheet writeSheet;
        for (ReadSheet sheet : sheets) {
            writeSheet = EasyExcel.writerSheet(sheet.getSheetNo(), sheet.getSheetName()).build();
            excelWriter.write(results.get(sheet.getSheetNo()), writeSheet);
        }
        excelWriter.finish();
    }

}

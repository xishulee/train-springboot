package xishu.core;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.write.metadata.WriteSheet;
import org.springframework.util.StringUtils;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @ClassName YamlTest
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/25
 * @Version
 **/
public class YamlTest {
    protected static String ROOT_PATH = "C:\\Users\\xuxigang\\Desktop\\正式";
    private static String projectPath = YamlTest.ROOT_PATH + "\\PMS3.0统推版本服务清单.xlsx";
    private static List<ServiceYaml> directors = new ArrayList<>();

    public static void main(String[] args) throws Exception {
        YamlTest yamlTest = new YamlTest();
        List<Project> projects = EasyExcel.read(YamlTest.projectPath).head(Project.class).sheet().doReadSync();
        Map<String, Project> projectMaps = projects.stream().filter(s -> !StringUtils.isEmpty(s.getPerson()))
                .collect(Collectors.toMap(Project::getEnName, Function.identity()));

        String configDir = YamlTest.ROOT_PATH + "\\配置文件";
        File defaultGroup = new File(configDir);
        if (defaultGroup.isDirectory()) {
            List<List<MatchResult>> results = new ArrayList<>();
            List<SheetInfo> sheetInfos = new ArrayList<>();
            File[] files = defaultGroup.listFiles();
            int index = 0;
            for (File file : files) {
                String provinceDir = file.getAbsolutePath() + "\\DEFAULT_GROUP";
                List<ServiceFileType> fileTypes = yamlTest.parseProvince(provinceDir);
                results.add(yamlTest.prepareSheetData(fileTypes, projectMaps));
                sheetInfos.add(new SheetInfo(index++, file.getName().substring(0, 2)));
            }

            yamlTest.export(sheetInfos, results);
        }
    }

    private void export(List<SheetInfo> sheetInfos, List<List<MatchResult>> results) {
        String resultFileName = YamlTest.ROOT_PATH +"\\result" + System.currentTimeMillis() + ".xlsx";
        ExcelWriter excelWriter = EasyExcel.write(resultFileName).build();
        WriteSheet writeSheet;
        for (SheetInfo sheet : sheetInfos) {
            writeSheet = EasyExcel.writerSheet(sheet.getSheetNum(), sheet.getSheetName()).head(MatchResult.class).build();
            excelWriter.write(results.get(sheet.getSheetNum()), writeSheet);
        }
        writeSheet = EasyExcel.writerSheet(sheetInfos.size(), "缺负责人").head(ServiceYaml.class).build();
        excelWriter.write(directors, writeSheet);
        excelWriter.finish();
    }

    /**
     * 准备sheet页签数据
     *
     * @param serviceFileTypes
     * @param projectMaps
     * @return
     */
    private List<MatchResult> prepareSheetData(List<ServiceFileType> serviceFileTypes, Map<String, Project> projectMaps) {
        List<MatchResult> matchResults = new ArrayList<>();
        for (ServiceFileType serviceFileType : serviceFileTypes) {
            System.out.println(serviceFileType.getYmlFileName() + " " + serviceFileType.getFileType());
            MatchResult matchResult = new MatchResult();
            matchResult.setYmlFileName(serviceFileType.getYmlFileName());
            matchResult.setFileType(serviceFileType.getFileType());
            int length = serviceFileType.getYmlFileName().length();
            Project orDefault = projectMaps.getOrDefault(serviceFileType.getYmlFileName().substring(0, length - 4), null);
            if (orDefault != null) {
                matchResult.setPerson(orDefault.getPerson());
                matchResult.setZhName(orDefault.getZhName());
                matchResult.setEnName(orDefault.getEnName());
            } else {
                ServiceYaml serviceYaml = new ServiceYaml(serviceFileType.getYmlFileName());
                if (!directors.contains(serviceYaml)) {
                    directors.add(serviceYaml);
                }
            }
            matchResults.add(matchResult);
        }

        return matchResults;
    }

    private List<ServiceFileType> parseProvince(String provinceDir) throws Exception {
        List<ServiceFileType> fileTypes = new ArrayList<>();
        File defaultGroup = new File(provinceDir);
        if (defaultGroup.isDirectory()) {
            File[] files = defaultGroup.listFiles(new FileFilter() {
                @Override
                public boolean accept(File path) {
                    return !path.getName().contains("bak");
                }
            });
            for (File file : files) {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line = bufferedReader.readLine();
                while (line != null) {
                    line = bufferedReader.readLine();
                    if (line != null && !line.equals("")
                            && line.trim().contains("conditionType")
                            && !line.trim().contains("#")) {
                        String trimLine = line.trim();
                        ServiceFileType serviceFileType = new ServiceFileType();
                        serviceFileType.setFileType(trimLine.substring(trimLine.indexOf(":") + 1).trim());
                        serviceFileType.setYmlFileName(file.getName());
                        fileTypes.add(serviceFileType);
                        break;
                    }
                }
            }
        }
        return fileTypes;
    }
}


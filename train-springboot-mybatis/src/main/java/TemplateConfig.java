import lombok.Data;

/**
 * @ClassName TemplateConfig
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/9/26
 * @Version
 **/

@Data
public class TemplateConfig {
    private String widgetId;
    private String widgetName;
    private String widgetCaption;
    private String widgetUrl;
    private String widgetHeight;
}

package com.xishulee.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication(scanBasePackages = {"com","generator"})
@MapperScan({"com.xishulee.mybatis.mapper","generator"})
public class TrainSpringbootMybatisApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(TrainSpringbootMybatisApplication.class, args);
        String property = applicationContext.getEnvironment().getProperty("spring.application.name");
        System.out.println(property);
    }

}

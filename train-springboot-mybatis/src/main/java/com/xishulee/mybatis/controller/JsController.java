package com.xishulee.mybatis.controller;

import com.xishulee.mybatis.service.JsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @ClassName JsController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/3/8
 * @Version
 **/
@RestController
@RequestMapping("/js")
public class JsController {

    @Autowired
    private JsService jsService;

    @GetMapping("/query")
    public ModelMap query(@RequestParam String sql){
        List<Map<String, Object>> lists = jsService.query(sql);
        ModelMap modelMap = new ModelMap();
        modelMap.put("data",lists);
        return modelMap;
    }

}

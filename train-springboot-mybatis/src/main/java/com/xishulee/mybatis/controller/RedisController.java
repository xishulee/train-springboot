package com.xishulee.mybatis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RedisController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/12/17
 * @Version
 **/
@RestController
@RequestMapping("/redis")
public class RedisController {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping
    public ModelMap testRedis(){
        ModelMap modelMap = new ModelMap();
        redisTemplate.opsForValue().set("test_name","xuxigang");
        modelMap.put("data", redisTemplate.opsForValue().get("test_name"));
        return modelMap;
    }
}

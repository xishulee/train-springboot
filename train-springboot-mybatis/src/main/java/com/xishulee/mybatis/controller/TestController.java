package com.xishulee.mybatis.controller;

import com.xishulee.mybatis.entity.TestVo;
import lombok.val;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * @ClassName TestController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/3/31
 * @Version
 **/

@RestController
@RequestMapping("/test2")
public class TestController {

    @GetMapping("export")
    public ModelMap test(@ModelAttribute TestVo vo){
        ModelMap modelMap = new ModelMap();
        modelMap.put("data",vo);
        return modelMap;
    }
}

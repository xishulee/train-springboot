package com.xishulee.mybatis.controller;

import com.xishulee.mybatis.entity.User;
import com.xishulee.mybatis.mapper.UserMapper;
import generator.service.SmRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName UserController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/2/23
 * @Version
 **/
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SmRoleService smRoleService;

    @GetMapping()
    public List<User> findUsers() {
        return userMapper.selectList(null);
    }

    @GetMapping("/test")
    public String find(){
        smRoleService.findByName("");
        return "xx";
    }
}

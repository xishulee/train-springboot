package com.xishulee.mybatis.entity;

import lombok.Data;

/**
 * @ClassName TestVo
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/3/31
 * @Version
 **/
@Data
public class TestVo {

    private long id;
    private String[] status;
}

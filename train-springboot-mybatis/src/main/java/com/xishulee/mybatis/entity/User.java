package com.xishulee.mybatis.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("sm_user")
public class User {
    @TableId
    private Long id;
    private String code;
    private Date accessTime;
    private String pwd;
    @TableField("is_system")
    private boolean system;
    private String state;
}

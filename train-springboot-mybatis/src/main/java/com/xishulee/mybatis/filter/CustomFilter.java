package com.xishulee.mybatis.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * @ClassName CustomFilter
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/3/26
 * @Version
 **/
@WebFilter("CustomFilter")
public class CustomFilter implements Filter{
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("CustomFilter");
        filterChain.doFilter(servletRequest,servletResponse);
    }
}

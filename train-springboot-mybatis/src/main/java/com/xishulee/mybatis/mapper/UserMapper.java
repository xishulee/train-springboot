package com.xishulee.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xishulee.mybatis.entity.User;

/**
 * @ClassName UserMapper
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/1/26
 * @Version
 **/
public interface UserMapper extends BaseMapper<User> {
}

package com.xishulee.mybatis.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCountCallbackHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @ClassName JsService
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/3/8
 * @Version
 **/
@Service
@DS("js")
public class JsService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @Transactional(readOnly = true)
    public List<Map<String, Object>> query(String sql){
        List<Map<String, Object>> mapList = jdbcTemplate.queryForList(sql);
        //根据sql算出总数
        //找出表信息中的表，取得字段备注
        RowCountCallbackHandler rowCountCallbackHandler = new RowCountCallbackHandler();
        jdbcTemplate.query(sql,rowCountCallbackHandler);
        System.out.println("行数：" + rowCountCallbackHandler.getRowCount());
        System.out.println("列数：" + rowCountCallbackHandler.getColumnCount());
        String[] columnNames = rowCountCallbackHandler.getColumnNames();
//        for(String colName : columnNames){
//            System.out.println(colName);
//        }
//        this.getTableColInfo("sm_user");
        //拼接分页的查询语法 limit pageSize  offset pageNum
        //pageNum 默认为0
        return mapList;
    }

    private Map<String,String> getTableColInfo(String tableName){
        try {
            Connection connection = dataSource.getConnection();
            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet metaDataTables = databaseMetaData.getTables(null, "%", tableName, new String[]{"TABLE"});
            while(metaDataTables.next()){
                String table_name = metaDataTables.getString("TABLE_NAME");
                System.out.println(table_name);
                if(table_name.equalsIgnoreCase(tableName)){
//                    connection.getMetaData().getColumns(null,getsche)
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

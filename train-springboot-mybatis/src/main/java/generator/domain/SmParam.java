package generator.domain;

import java.io.Serializable;

/**
 * 
 * @TableName sm_param
 */
public class SmParam implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private Integer paramClassify;

    /**
     * 
     */
    private String desciption;

    /**
     * 
     */
    private String paramKey;

    /**
     * 
     */
    private String paramValue;

    /**
     * 
     */
    private String paramDisValue;

    /**
     * 
     */
    private String paramType;

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 
     */
    public Integer getParamClassify() {
        return paramClassify;
    }

    /**
     * 
     */
    public void setParamClassify(Integer paramClassify) {
        this.paramClassify = paramClassify;
    }

    /**
     * 
     */
    public String getDesciption() {
        return desciption;
    }

    /**
     * 
     */
    public void setDesciption(String desciption) {
        this.desciption = desciption;
    }

    /**
     * 
     */
    public String getParamKey() {
        return paramKey;
    }

    /**
     * 
     */
    public void setParamKey(String paramKey) {
        this.paramKey = paramKey;
    }

    /**
     * 
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * 
     */
    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    /**
     * 
     */
    public String getParamDisValue() {
        return paramDisValue;
    }

    /**
     * 
     */
    public void setParamDisValue(String paramDisValue) {
        this.paramDisValue = paramDisValue;
    }

    /**
     * 
     */
    public String getParamType() {
        return paramType;
    }

    /**
     * 
     */
    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SmParam other = (SmParam) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getParamClassify() == null ? other.getParamClassify() == null : this.getParamClassify().equals(other.getParamClassify()))
            && (this.getDesciption() == null ? other.getDesciption() == null : this.getDesciption().equals(other.getDesciption()))
            && (this.getParamKey() == null ? other.getParamKey() == null : this.getParamKey().equals(other.getParamKey()))
            && (this.getParamValue() == null ? other.getParamValue() == null : this.getParamValue().equals(other.getParamValue()))
            && (this.getParamDisValue() == null ? other.getParamDisValue() == null : this.getParamDisValue().equals(other.getParamDisValue()))
            && (this.getParamType() == null ? other.getParamType() == null : this.getParamType().equals(other.getParamType()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getParamClassify() == null) ? 0 : getParamClassify().hashCode());
        result = prime * result + ((getDesciption() == null) ? 0 : getDesciption().hashCode());
        result = prime * result + ((getParamKey() == null) ? 0 : getParamKey().hashCode());
        result = prime * result + ((getParamValue() == null) ? 0 : getParamValue().hashCode());
        result = prime * result + ((getParamDisValue() == null) ? 0 : getParamDisValue().hashCode());
        result = prime * result + ((getParamType() == null) ? 0 : getParamType().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", paramClassify=").append(paramClassify);
        sb.append(", desciption=").append(desciption);
        sb.append(", paramKey=").append(paramKey);
        sb.append(", paramValue=").append(paramValue);
        sb.append(", paramDisValue=").append(paramDisValue);
        sb.append(", paramType=").append(paramType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
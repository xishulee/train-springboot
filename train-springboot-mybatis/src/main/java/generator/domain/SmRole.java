package generator.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName sm_role
 */
@TableName(value ="sm_role")
@Data
public class SmRole implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long pmRoleId;

    /**
     * 
     */
    private Boolean relation;

    /**
     * 
     */
    private String note;

    /**
     * 
     */
    private Boolean inherit;

    /**
     * 
     */
    private String name;

    /**
     * 
     */
    private Boolean isSystem;

    /**
     * 
     */
    private Integer roleType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
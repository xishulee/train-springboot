package generator.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;

import generator.domain.SmRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author xuxigang
* @description 针对表【sm_role】的数据库操作Mapper
* @createDate 2022-01-27 11:13:06
* @Entity generator.domain.SmRole
*/
public interface SmRoleMapper extends BaseMapper<SmRole> {

    /**
     * 根据角色名称查找
     * @param name
     * @return
     */
    List<SmRole> selectByName(@Param("name") String name);

    /**
     * 根据角色名称查找
     * @param name
     * @return
     */
    List<SmRole> findByName(@Param("name") String name);

    /**
     * 根据角色名称查找
     * @param name
     * @return
     */
    SmRole findOneByName(@Param("name") String name);
}





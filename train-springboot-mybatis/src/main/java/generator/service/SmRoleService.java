package generator.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import generator.domain.SmRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author xuxigang
* @description 针对表【sm_role】的数据库操作Service
* @createDate 2022-01-27 11:13:06
*/
@DS("js")
public interface SmRoleService extends IService<SmRole> {

    /**
     * 根据名字查找
     * @param name
     * @return
     */
    SmRole findByName(String name);
}

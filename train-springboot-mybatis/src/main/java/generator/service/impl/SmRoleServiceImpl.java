package generator.service.impl;

import com.baomidou.mybatisplus.core.conditions.AbstractWrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import generator.domain.SmRole;
import generator.service.SmRoleService;
import generator.mapper.SmRoleMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xuxigang
 * @description 针对表【sm_role】的数据库操作Service实现
 * @createDate 2022-01-27 11:13:06
 */
@Service
public class SmRoleServiceImpl extends ServiceImpl<SmRoleMapper, SmRole>
        implements SmRoleService {

    @Override
    public SmRole findByName(String name) {
        QueryWrapper<SmRole> queryWrapper = new QueryWrapper<>();
        getBaseMapper().selectOne(queryWrapper.eq("name", "xxx"));


        LambdaQueryWrapper<SmRole> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SmRole::getName, "xx");
        getBaseMapper().selectOne(lambdaQueryWrapper);
        getBaseMapper().findOneByName("xx");
        return getBaseMapper().findOneByName(name);
    }
}





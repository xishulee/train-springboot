
create table "sm_user" (
   id int8 not null,
   access_count int4 not null,
   code varchar(40) not null,
   enabled char(1),
   freeze_time timestamp,
   access_time timestamp,
   login_fail_count int4 not null,
   pwd varchar(40) not null,
   person_id varchar(20) not null,
   is_system char(1) not null,
   state int4 not null,
   check_remark varchar(150),
   user_type int4,
   primary key (id)
);
create index sm_user_code on sm_user using btree (code);


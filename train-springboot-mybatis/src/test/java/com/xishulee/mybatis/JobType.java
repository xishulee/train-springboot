package com.xishulee.mybatis;

import lombok.Data;

/**
 * @ClassName JobType
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/8/27
 * @Version
 **/
@Data
public class JobType {
    private String code;
    private String name;
    private String seqNo;

    public JobType(String code, String name, String seqNo){
        this.code = code;
        this.name = name;
        this.seqNo = seqNo;
    }
}

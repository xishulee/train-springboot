package com.xishulee.mybatis;

import lombok.val;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * @ClassName T
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/2/25
 * @Version
 **/
public class T {
    private int a;
    public static void main(String[] args) throws MalformedURLException, URISyntaxException {
        T t = new T(10);
        //编译错误
//        t.a;

        URI url = new URI("www.baidu.com");
        System.out.println(url.getScheme());
        System.out.println(url.toURL());
    }

    T(int a){
        this.a = a;
    }
}

package com.xishulee.mybatis;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @ClassName Test
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/8/27
 * @Version
 **/
public class Test {
    public static void main(String[] args) {
        List<JobType> jobTypeList = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            jobTypeList.add(new JobType("code" + i, "name" + i, String.valueOf(new Random(100).nextInt())));
        }
        jobTypeList.stream();
    }
}

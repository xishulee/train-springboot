package com.xishulee.mybatis;

import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.xishulee.mybatis.entity.User;
import com.xishulee.mybatis.mapper.UserMapper;
import com.xishulee.mybatis.mapper.UserMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 使用junit进行测试时，必须设置@RunWith(SpringRunner.class)
 * 否则userMapper是空
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TrainSpringbootMybatisApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    public void contextLoads() {
        if(userMapper == null) {
            System.out.printf("null");
        }else{
            System.out.printf("not null");
        }
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        Assert.assertEquals(5, userList.size());
        userList.forEach(System.out::println);

//        userMapper.selectPage()

        PaginationInnerInterceptor a;
    }

}

package com.xishulee.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainSpringbootOssApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainSpringbootOssApplication.class, args);
    }

}

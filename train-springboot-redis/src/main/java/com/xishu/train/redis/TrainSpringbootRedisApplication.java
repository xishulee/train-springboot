package com.xishu.train.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainSpringbootRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainSpringbootRedisApplication.class, args);
    }

}

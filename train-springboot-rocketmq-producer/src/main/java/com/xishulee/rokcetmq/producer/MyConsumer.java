package com.xishulee.rokcetmq.producer;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyConsumer
 * @Description: TODO
 * @Author xuxigang
 * @Date 2024/2/8
 * @Version
 **/
@Component
@RocketMQMessageListener(topic = "test1",consumerGroup = "springboot_consumer_group")
public class MyConsumer implements RocketMQListener<String> {
    @Override
    public void onMessage(String message) {
        System.out.printf("Received message is " + message);
    }
}

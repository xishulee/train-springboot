package com.xishulee.rokcetmq.producer;

import lombok.Setter;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName ProducerController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2024/2/8
 * @Version
 **/
@RestController
public class ProducerController {

    @Setter(onMethod_ = @Autowired)
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/make")
    private ModelMap test(@RequestParam String name) {
        Message<String> helloRocketMQ = MessageBuilder.withPayload("Hello RocketMQ ,".concat(name)).build();
        ModelMap modelMap = new ModelMap();

        try {
            rocketMQTemplate.send("test1", helloRocketMQ);
            modelMap.put("success", true);
        } catch (Exception ex) {
            modelMap.put("success", false);
        }
        return modelMap;

    }
}

package xishu.starter;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 如果定义了Enable*，则不需要在spring.factories中指定自动配置的类,
 * 如果两边都配置了，也没有关系
 * 如果只有Enable*,则需要在启动的地方增加@Enable*注解,显示启动配置
 * 两者的区别可能就是一个显示启动，一个默认启动，注解的模式给使用者一个选择的机会
 */
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({UserAutoConfigure.class})
public @interface EnableUserClient {

}

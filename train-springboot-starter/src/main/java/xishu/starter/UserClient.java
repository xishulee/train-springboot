package xishu.starter;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @ClassName UserClient
 * @Description: TODO
 * @Author xuxigang
 * @Date 2020/2/29
 * @Version
 **/
public class UserClient {
    @Autowired
    private UserProperties userProperties;

    /**
     *
     * @return
     */
    public String getName(){
        return userProperties.getName();
    }
}

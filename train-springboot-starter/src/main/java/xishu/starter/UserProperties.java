package xishu.starter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName UserProperties
 * @Description: TODO
 * @Author xuxigang
 * @Date 2020/2/29
 * @Version
 **/
@Data
@ConfigurationProperties("xishu.user")
public class UserProperties {
    private String name;
}

package xishu.web;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyApplicationRun
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/5/17
 * @Version
 **/
@Component
public class MyApplicationRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("MyApplicationRunner");
        System.out.println(args.toString());
    }
}

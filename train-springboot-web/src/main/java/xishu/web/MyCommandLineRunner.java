package xishu.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName MyCommandLineRunner
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/5/17
 * @Version
 **/
@Component
public class MyCommandLineRunner implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        System.out.println("command line args");
        System.out.println(args);
    }
}

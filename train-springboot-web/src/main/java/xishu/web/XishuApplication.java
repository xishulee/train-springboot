package xishu.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.context.support.WebApplicationContextUtils;
import xishu.core.jpa.BaseRepository;
import xishu.starter.EnableUserClient;

/**
 * Created by xuxigang on 2018/7/26.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"xishu.web.dao"},repositoryBaseClass= BaseRepository.class)
@EnableUserClient
//@EnableDiscoveryClient
public class XishuApplication{

    public static void main(String[] args) throws Exception {
        System.out.println("start");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(XishuApplication.class, args);
        System.out.println("ddo.....");
        StringRedisTemplate stringRedisTemplate = applicationContext.getBean(StringRedisTemplate.class);
        RedisConnection connection = stringRedisTemplate.getConnectionFactory().getConnection();
        connection.subscribe(new MessageListener() {
            @Override
            public void onMessage(Message message, byte[] bytes) {
                byte [] bodyBytes = message.getBody();
                System.out.println( new String(bodyBytes));
                
//                System.out.println(new String(bodyBtyes));
//                System.out.println(bytes.toString());
//                System.out.println(new String(bytes));

            }
        }, "chan1".getBytes());
    }

}

package xishu.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import xishu.core.jpa.BaseRepository;

/**
 * 配置deployable war , pom.xml同时需要修改<packaging>war</packaging>
 * Created by xuxigang on 2018/7/26.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"xishu.web.dao"},repositoryBaseClass= BaseRepository.class)
public class XishuWebApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(XishuApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(XishuWebApplication.class, args);
    }

}

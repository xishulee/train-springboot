package xishu.web.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import xishu.web.controller.redis.ApiGatewayRouteChangedListener;

import javax.annotation.Resource;

/**
 * @ClassName RedisConfig
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/2/19
 * @Version
 **/
@Configuration
public class RedisConfig {

    @Resource
    private RedisConnectionFactory redisConnectionFactory;

    @Bean
    public RedisMessageListenerContainer redisMessageListenerContainer(){
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(redisConnectionFactory);
        container.addMessageListener(messageListenerAdapter() , new PatternTopic(""));
        return container;
    }

    @Bean
    public ApiGatewayRouteChangedListener apiGatewayRouteChangedListener(){
        return new ApiGatewayRouteChangedListener();
    }

    @Bean
    MessageListenerAdapter messageListenerAdapter() {
        System.out.println("消息适配器1");
        return new MessageListenerAdapter(apiGatewayRouteChangedListener(), "onMessage");
    }

    @Bean
    StringRedisTemplate pubRedisTemplate() {
        return new StringRedisTemplate(redisConnectionFactory);
    }

}

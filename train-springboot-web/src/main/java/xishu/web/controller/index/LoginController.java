package xishu.web.controller.index;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by xuxigang on 2018/7/26.
 */
@RestController
public class LoginController {

    @PostMapping("/login")
    @ResponseBody
    public ModelMap login(@RequestBody LoginQuery loginForm, HttpSession httpSession) {
        return null;
    }

    @PostMapping("/logout")
    @ResponseBody
    public ModelMap logout(@RequestParam Long userId, HttpSession httpSession) {
        return null;
    }

}

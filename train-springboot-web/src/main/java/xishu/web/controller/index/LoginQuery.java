package xishu.web.controller.index;

import java.io.Serializable;

/**
 * Created by xuxigang on 2018/7/26.
 * 登录信息
 */
public class LoginQuery implements Serializable {
    private String userCode;
    private String password;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

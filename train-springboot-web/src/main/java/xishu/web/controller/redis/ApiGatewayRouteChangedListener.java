package xishu.web.controller.redis;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * @ClassName ApiGatewayRouteChangedListener
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/4/8
 * @Version
 **/
public class ApiGatewayRouteChangedListener implements MessageListener {

    /**
     * @param message 消息体
     * @param bytes 频道
     */
    @Override
    public void onMessage(Message message, byte[] bytes) {

    }
}

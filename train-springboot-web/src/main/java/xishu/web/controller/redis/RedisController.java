package xishu.web.controller.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName RedisController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/2/19
 * @Version
 **/
@RestController
@RequestMapping("redis")
public class RedisController {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @GetMapping("sds")
    public ModelMap addString() {
        ModelMap map = new ModelMap();
        stringRedisTemplate.opsForValue().set("user:name", "xuxigang");
        String name = stringRedisTemplate.opsForValue().get("user:name");
        map.put("name", name);
        return map;
    }
}

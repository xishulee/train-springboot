package xishu.web.controller.role;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import xishu.core.FishConstant;
import xishu.core.form.HttpJsonResult;
import xishu.web.dao.RoleDao;
import xishu.web.entity.Role;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/7/26.
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Autowired
    private RoleDao roleDao;

    @GetMapping("/query")
    public HttpJsonResult search(Pageable pageable, @ModelAttribute RoleQuery roleQuery){
        HttpJsonResult httpJsonResult = null;
        StringBuilder hql = new StringBuilder();
        hql.append("select role from Role role");
        try {
            Page<Map> page = roleDao.findPageByParams(hql.toString(), pageable/*, "%"+roleQuery.getName()+"%"*/);
            httpJsonResult = new HttpJsonResult(page);
        }catch (Exception ex){
            ex.printStackTrace();
            httpJsonResult = new HttpJsonResult(ex.getMessage());
        }
        return httpJsonResult;
    }

    @GetMapping()
    public HttpJsonResult searchRoles(Pageable pageable, @ModelAttribute RoleQuery query, @RequestParam MultiValueMap<String, String> parameters){
        HttpJsonResult httpJsonResult = null;
        ModelMap modelMap = new ModelMap();
        try {
            Page<Role> page = roleDao.findAll(new Specification<Role>() {//查询条件构造
                @Override
                public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                    List<Predicate> roleList = new ArrayList<Predicate>();
                    String name = parameters.getFirst("name");
                    if(!StringUtils.isEmpty(name)){
                        roleList.add(cb.like(root.get("name").as(String.class), "%"+name+"%"));
                    }
                    Predicate[] p = new Predicate[roleList.size()];

                    return cb.and(roleList.toArray(p));
                }
            },pageable);
            httpJsonResult = new HttpJsonResult(page);
        }catch (Exception ex){
            logger.error(ex.getMessage());
            httpJsonResult = new HttpJsonResult(ex.getMessage());
        }
        return httpJsonResult;
    }

    /**
     * 增加角色
     * @param role
     * @return
     */
    @PostMapping
    public ModelMap addRole(@RequestBody Role role){
        ModelMap modelMap = new ModelMap();
        try {
            Role entity = roleDao.saveAndFlush(role);
            modelMap.put(FishConstant.SUCCESS,true);
            modelMap.put(FishConstant.DATA,entity);
        }catch (Exception ex){
            modelMap.put(FishConstant.SUCCESS,false);
            modelMap.put(FishConstant.ERROR_MSG,"保存角色失败");
        }
        return modelMap;
    }

    /**
     * 修改角色
     * @param role
     * @return
     */
    @PutMapping
    public ModelMap updateRole(@RequestBody Role role){
        ModelMap modelMap = new ModelMap();
        try {
            Role entity = roleDao.saveAndFlush(role);
            modelMap.put(FishConstant.SUCCESS,true);
            modelMap.put(FishConstant.DATA,entity);
        }catch (Exception ex){
            modelMap.put(FishConstant.SUCCESS,false);
            modelMap.put(FishConstant.ERROR_MSG,"修改角色失败");
        }
        return modelMap;
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public @ResponseBody ModelMap deleteRole(@PathVariable long id){
        ModelMap modelMap = new ModelMap();
        try{
            roleDao.deleteById(id);
            modelMap.put(FishConstant.SUCCESS,true);
        }catch (Exception ex){
            modelMap.put(FishConstant.SUCCESS,false);
            modelMap.put(FishConstant.ERROR_MSG,ex.getMessage());
            logger.error(ex.getMessage());
        }
        return modelMap;
    }
}

package xishu.web.controller.role;

import xishu.core.query.BaseQuery;

/**
 * Created by xuxigang on 2018/7/25.
 */
public class RoleQuery extends BaseQuery {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

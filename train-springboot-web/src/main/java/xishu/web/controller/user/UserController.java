package xishu.web.controller.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import xishu.core.form.HttpJsonResult;
import xishu.starter.UserClient;
import xishu.web.dao.UserDao;
import xishu.web.entity.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/7/29.
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserDao userDao;

    @GetMapping()
    public HttpJsonResult searchUsers(Pageable pageable, @RequestParam MultiValueMap<String, String> parameters){
        HttpJsonResult httpJsonResult = null;
        ModelMap modelMap = new ModelMap();
        try {
            Page<User> page = userDao.findAll(new Specification<User>() {//查询条件构造
                @Override
                public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                    List<Predicate> roleList = new ArrayList<Predicate>();
                    String name = parameters.getFirst("name");
                    if(!StringUtils.isEmpty(name)){
                        roleList.add(cb.like(root.get("name").as(String.class), "%"+name+"%"));
                    }
//                    Predicate[] p = new Predicate[roleList.size()];

                    return cb.and(roleList.toArray(new Predicate[roleList.size()]));
                }
            },pageable);
            httpJsonResult = new HttpJsonResult(page);
        }catch (Exception ex){
            logger.error(ex.getMessage());
            httpJsonResult = new HttpJsonResult(ex.getMessage());
        }
        return httpJsonResult;
    }

    @GetMapping("/u")
    public ModelMap searchUsers(){
        ModelMap modelMap = new ModelMap();
        List<Map> users = userDao.findByParams("select user from User user");
        modelMap.put("data",users);
        return modelMap;
    }

    @Autowired
    private UserClient userClient;

    @GetMapping("/name")
    public String getUserName(){
        return userClient.getName();
    }


}

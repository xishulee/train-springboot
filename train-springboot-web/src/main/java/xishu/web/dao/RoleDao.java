package xishu.web.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import xishu.core.jpa.IBaseRepository;
import xishu.web.entity.Role;

/**
 * Created by xuxigang on 2018/7/26.
 */
@Repository
public interface RoleDao extends IBaseRepository<Role,Long> {

    @Query("select role from Role role where role.id = :id")
    public void findTest(@Param("id") int id);

}

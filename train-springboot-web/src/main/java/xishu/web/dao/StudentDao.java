package xishu.web.dao;

import org.springframework.stereotype.Repository;
import xishu.core.jpa.IBaseRepository;
import xishu.web.entity.Student;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
@Repository
public interface StudentDao extends IBaseRepository<Student,Long> {
}

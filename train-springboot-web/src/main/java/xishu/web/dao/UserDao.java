package xishu.web.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import xishu.core.form.DropDownVo;
import xishu.core.jpa.IBaseRepository;
import xishu.web.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/7/29.
 */
@Repository
public interface UserDao extends IBaseRepository<User,Long>{

    /**
     * 动态查询，实体之间没有关联关系
     * @param r
     * @param pageable
     * @return
     * @TODO 还没有找到如何在Specification中设置role.name的条件的方式，如果不是在Specification中设置还是可以的
     */
    @Query("select user from User user left join Org org on user.org.id = org.id left join Role role on role.id = user.roleId")
    Page<User> findPage(Specification<User> r, Pageable pageable);

    /**
     * 动态查询，主查询实体内存在多对一的关系
     * @param r
     * @param pageable
     * @return
     */
    @Override
    Page<User> findAll(Specification<User> r,Pageable pageable);

    /**
     * 存在关联关系的实体之间1条sql查询出结果，结果保存在Object[]数组中
     * Object[] o = (Object[])list.get(0);
     * User user = (User)o[0];
     * Org org = (Org)o[1];
     * @return
     */
    @Query("select user,org from User user left outer join Org org on user.org.id = org.id")
    List<Object[]> findUser();

    /**
     *  获取实体的部分属性并返回一个map
     * @return
     */
    @Query("select new map (u.name as name,u.code as code,u.id as id) from User u")
    List<Map<String,Object>> findYihua();

    /**
     * 获取实体的部分属性，并返回一个非实体类
     * jql返回的属性生成一个临时新类(在这里sql不能生成新类)
     * @return
     */
    @Query(value = "select new xishu.core.form.DropDownVo(u.id,u.code,u.name) from User u")
    List<DropDownVo> findDropDown();

    /**
     * 原生sql的多表关联查询
     * @return Object或者Object[]，其他类型不行
     */
    @Query(value = "select u.*,org.org_code as orgCode, org.org_name as orgName from sm_user u left join sm_org org on u.org_id = org.id",nativeQuery = true)
    List<Object[]> findUsersBySql();

    /**
     * 原生的sql查询，返回User对象
     * 当调用user.getOrg()时会触发查找机构的一条语句
     * @return
     */
    @Query(value = "select u.* from sm_user u ",nativeQuery = true)
    List<User> findUsers();

}

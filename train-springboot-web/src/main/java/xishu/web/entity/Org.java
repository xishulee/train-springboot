package xishu.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 机构实体
 *
 * @author YH-HJ
 * @since 2017-5-27
 */
@Entity
@Table(name = "SM_ORG")
public class Org {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "SEQ_SM_ORG")
    @SequenceGenerator(name = "SEQ_SM_ORG", sequenceName = "SEQ_SM_ORG")
    @Column(name = "ID")
    private long id;

    /**
     * 机构编码
     */
    @Column(name = "ORG_CODE")
    private String orgCode;

    /**
     * 机构名称
     */
    @Column(name = "ORG_NAME")
    private String orgName;

    /**
     * 机构所在地址
     */
    @Column(name = "ORG_ADDRESS")
    private String orgAddress;

    /**
     * 机构邮政编号
     */
    @Column(name = "ORG_ZIP")
    private String orgZIP;

    /**
     * 机构类型
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ORG_TYPE")
    private OrgType orgType;

    /**
     * 父机构
     */
    @Column(name = "PARENT_ID")
    private long parentId;

    /**
     * 机构级别
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "ORG_LEVEL")
    private OrgLevel orgLevel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrgAddress() {
        return orgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        this.orgAddress = orgAddress;
    }

    public String getOrgZIP() {
        return orgZIP;
    }

    public void setOrgZIP(String orgZIP) {
        this.orgZIP = orgZIP;
    }

    public OrgType getOrgType() {
        return orgType;
    }

    public void setOrgType(OrgType orgType) {
        this.orgType = orgType;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public OrgLevel getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(OrgLevel orgLevel) {
        this.orgLevel = orgLevel;
    }
}

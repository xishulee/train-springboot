package xishu.web.entity;

/**
 * 机构级别
 * Created by xuxigang on 2018/7/29.
 */
public enum OrgLevel {
    GLOBAL,PROVICE,CITY,BRANCH
}

package xishu.web.entity;

/**
 * 机构类型
 * Created by xuxigang on 2018/7/29.
 */
public enum OrgType {
    BANK,SERVICE_PROVIDER
}

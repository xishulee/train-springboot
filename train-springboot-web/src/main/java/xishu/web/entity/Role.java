package xishu.web.entity;

import javax.persistence.*;


/**
 * 角色表（对应数据库表SM_ROLE）
 * @author LYC
 *
 */
@Entity
@Table(name = "SM_ROLE")
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO/*, generator = "SEQ_SM_ROLE"*/)
//	@SequenceGenerator(name = "SEQ_SM_ROLE", sequenceName = "SEQ_SM_ROLE")
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "IS_VALID")
	private String isValid;
	
	@Column(name = "NOTE")
	private String note;

	
	public Long getId() {
		return this.id;
	}

	
	public void setId(Long id) {
		this.id = id;
	}

	
	public String getName() {
		return this.name;
	}

	
	public void setName(String name) {
		this.name=name;
	}

	
	public String getCode() {
		return this.code;
	}

	
	public void setCode(String code) {
		this.code=code;
	}

	
	public String getIsValid() {
		return this.isValid;
	}

	
	public void setIsValid(String isValid) {
		this.isValid=isValid;
	}

	
	public String getNote() {
		return this.note;
	}

	public void setNote(String note) {
		this.note=note;
	}
	
}

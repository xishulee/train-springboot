package xishu.web.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
@Data
@Entity
@Table(name = "SM_STUDENT")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;
}

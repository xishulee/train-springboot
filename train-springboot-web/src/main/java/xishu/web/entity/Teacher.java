package xishu.web.entity;

import lombok.Data;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
@Data
public class Teacher {
    private long id;
    private String name;
}

package xishu.web.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;


/**
 * 账户信息:（信息实体对应数据库表SM_USER）
 */
@Entity
@Table(name = "SM_USER")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO/*, generator = "SEQ_SM_USER"*/)
//    @SequenceGenerator(name = "SEQ_SM_USER", sequenceName = "SEQ_SM_USER")
    @Column(name = "ID")
    private Long id;

    /**
     * 账号（使用人员信息的编号）
     */
    @Column(name = "CODE")
    private String code;

    @Column(name = "NAME")
    private String name;

    /**
     * 密码
     */
    @Column(name = "PWD")
    private String password;

    @Column(name = "ROLE_ID")
    private long roleId;

    @Column(name = "PHONE")
    private String phone;

    /**
     * 状态
     */
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "STATE")
    private UserState userState;

    /**
     * 是否启用
     */
    @Column(name = "ENABLED")
    @org.hibernate.annotations.Type(type = "yes_no")
    private boolean enabled;

    /**
     * 最后访问时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACCESS_TIME", nullable = true)
    private Date accessTime;

    /**
     * 登录错误次数
     */
    @Column(name = "LOGIN_FAIL_COUNT")
    private int count;

    /**
     * 冻结时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FREEZE_TIME")
    private Date freeze;

    @ManyToOne(targetEntity = Org.class)
    @JoinColumn(name = "ORG_ID")
    @Fetch(FetchMode.JOIN)//CAN'T WORK
    private Org org;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public UserState getState() {
        return this.userState;
    }

    public void setState(UserState state) {
        this.userState = state;
    }

    public Date getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(Date accessTime) {
        this.accessTime = accessTime;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return this.name;
    }

    public UserState getUserState() {
        return this.userState;
    }

    public Date getLoginTime() {
        return this.accessTime;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setLogoutTime() {
        this.accessTime = new Date();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getFreeze() {
        return freeze;
    }

    public void setFreeze(Date freeze) {
        this.freeze = freeze;
    }

    public Org getOrg() {
        return org;
    }

    public void setOrg(Org org) {
        this.org = org;
    }
}
package xishu.web.service.role;

import com.querydsl.core.QueryResults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import xishu.web.controller.role.RoleQuery;
import xishu.web.entity.Role;

import java.util.List;

/**
 * 角色服务
 * Created by xuxigang on 2018/8/9.
 */
public interface IRoleService {

    /**
     * 根据Id查找角色
     * @param id
     * @return
     */
    Role getRoleById(Long id);

    /**
     * 插入角色
     * @param role
     */
    void save(Role role);

    /**
     * 修改角色
     * @param role
     */
    void update(Role role);

    /**
     * 删除角色
     * @param role
     */
    void delete(Role role);

    /**
     * 根据Id删除角色
     * @param id
     */
    void delete(Long id);

    /**
     * 根据条件查找角色
     * @param roleQuery
     * @return
     */
    List<Role> findRoles(RoleQuery roleQuery);

    /**
     * 分页显示角色信息
     * @param pageable
     * @param roleQuery
     * @return
     */
    QueryResults<Role> pageByQueryDSL(Pageable pageable, RoleQuery roleQuery);

    /**
     * 默认dao的实现
     * @param pageable
     * @param roleQuery
     * @return
     */
    Page<Role> page(Pageable pageable, RoleQuery roleQuery);

}

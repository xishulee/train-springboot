package xishu.web.service.role;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import xishu.web.controller.role.RoleQuery;
import xishu.web.dao.RoleDao;
import xishu.web.entity.QRole;
import xishu.web.entity.Role;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xuxigang on 2018/8/9.
 */
@Service
@Transactional(readOnly = true)
public class RoleService implements IRoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public Role getRoleById(Long id) {
        return roleDao.getOne(id);
    }

    @Override
    public void save(Role role) {
        roleDao.saveAndFlush(role);
    }

    @Override
    public void update(Role role) {
        roleDao.saveAndFlush(role);
    }

    @Override
    public void delete(Role role) {
        roleDao.delete(role);
    }

    @Override
    public void delete(Long id) {
        roleDao.deleteById(id);
    }

    @Override
    public List<Role> findRoles(RoleQuery roleQuery) {
        JPAQuery<Role> jpaQuery = roleDao.getJpaQuery();
        jpaQuery.select(QRole.role).from(QRole.role);

        return buildPredicate(jpaQuery, roleQuery)
                .fetch();
    }

    @Override
    public QueryResults<Role> pageByQueryDSL(Pageable pageable, RoleQuery roleQuery) {
        JPAQuery jpaQuery = roleDao.getJpaQuery();
        jpaQuery.select(QRole.role).from(QRole.role);

        buildPredicate(jpaQuery, roleQuery)
                .offset(pageable.getOffset())
                .limit(pageable.getPageNumber());;

        return jpaQuery.fetchResults();
    }

    @Override
    public Page<Role> page(Pageable pageable, RoleQuery roleQuery) {
        Predicate predicate = QRole.role.name.like(roleQuery.getName());
        return roleDao.findAll(predicate, pageable);
    }

    /**
     * 动态构建查询条件
     *
     * @param jpaQuery
     * @param roleQuery
     * @return
     */
    private JPAQuery buildPredicate(JPAQuery jpaQuery, RoleQuery roleQuery) {
        List<Predicate> predicates = new ArrayList<>();
        if (!StringUtils.isEmpty(roleQuery.getName())) {
            predicates.add(QRole.role.name.like(roleQuery.getName()));
        }
        if (!predicates.isEmpty()) {
            jpaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
        }
        return jpaQuery;
    }
}

package xishu.web.service.student;

import xishu.web.entity.Student;

import java.util.List;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
public interface IStudentService {

    /**
     * 查找所有的学生
     * @return
     */
    List<Student> findAll();

    /**
     * 增加一个学生
     * @param student
     * @return
     */
    Student add(Student student);

    /**
     * 模拟异常增加
     * @param student
     */
    void mockExceptionAdd(Student student);
}

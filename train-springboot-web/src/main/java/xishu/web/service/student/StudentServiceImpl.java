package xishu.web.service.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xishu.web.dao.StudentDao;
import xishu.web.entity.Role;
import xishu.web.entity.Student;
import xishu.web.service.role.IRoleService;

import java.util.List;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
@Service
public class StudentServiceImpl implements IStudentService {

    @Autowired
    StudentDao studentDao;

    @Override
    public List<Student> findAll() {
        return studentDao.findAll();
    }

    @Autowired
    IRoleService roleService;

    /**
     * @param student
     * @return
     */
    @Override
    @Transactional
    public Student add(Student student) {
        return studentDao.saveAndFlush(student);
    }

    /**
     * 经过测试，写不写rollbackFor = Exception.class都会回滚事务
     *
     * @param student
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void mockExceptionAdd(Student student) {
        Student entity = studentDao.save(student);
        String[] names = {"管理员", "什么玩意"};
        Role role = new Role();
        role.setCode("admin");
        role.setIsValid("1");
        //模拟数组下标越界异常
        role.setName(names[3]);
        roleService.save(role);
    }
}

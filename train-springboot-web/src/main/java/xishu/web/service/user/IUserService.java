package xishu.web.service.user;

import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import xishu.core.form.DropDownVo;
import xishu.web.entity.User;

import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/8/7.
 */
public interface IUserService {
    List<DropDownVo> findBySql();

    List<DropDownVo> findBySql1();

    List<Map> findListMapBySql();

    Page<User> page(Pageable pageable, MultiValueMap<String, String> parameters);

    Page<User> pageUser(Pageable pageable, MultiValueMap<String, String> parameters);

    Page<User> pageByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters);

    List<DropDownVo> findByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters);

    List<Tuple> pageTupleByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters);

    QueryResults<Tuple> pageTupleLeftJoinByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters);
}

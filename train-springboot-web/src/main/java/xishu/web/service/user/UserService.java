package xishu.web.service.user;

import com.google.common.collect.Lists;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import xishu.core.form.DropDownVo;
import xishu.web.dao.UserDao;
import xishu.web.entity.QOrg;
import xishu.web.entity.QRole;
import xishu.web.entity.QUser;
import xishu.web.entity.User;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/8/7.
 */
@Service
@Transactional(readOnly = true)
public class UserService implements IUserService {

    @Autowired
    private UserDao userDao;

    @Override
    public List<DropDownVo> findBySql() {
        String sql = "select id,code as value,name as text from sm_user";
        NativeQuery query = userDao.getNativeQuery(sql, DropDownVo.class);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("value", StandardBasicTypes.STRING);
        query.addScalar("text", StandardBasicTypes.STRING);
        return query.list();
    }

    @Override
    public List<DropDownVo> findBySql1() {
        String sql = "select id,code as value,name as text from sm_user";
        Map<String, Type> retColumnTypes = new HashMap<>();
        retColumnTypes.put("id", StandardBasicTypes.LONG);
        retColumnTypes.put("value", StandardBasicTypes.STRING);
        retColumnTypes.put("text", StandardBasicTypes.STRING);
        return userDao.findBySql(sql, DropDownVo.class, retColumnTypes);
    }

    @Override
    public List<Map> findListMapBySql() {
        String sql = "select id,code as value,name as text from sm_user";
        NativeQuery query = userDao.getNativeQuery(sql);
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.addScalar("id", StandardBasicTypes.LONG);
        query.addScalar("value", StandardBasicTypes.STRING);
        query.addScalar("text", StandardBasicTypes.STRING);
        return query.list();
    }

    @Override
    public Page<User> page(Pageable pageable, MultiValueMap<String, String> parameters){
        Page<User> page = userDao.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> roleList = new ArrayList<Predicate>();
                String name = parameters.getFirst("name");
                if(!StringUtils.isEmpty(name)){
                    roleList.add(cb.like(root.get("name").as(String.class), "%"+name+"%"));
                }
                String orgName = parameters.getFirst("orgName");
                if (!StringUtils.isEmpty(name)){
                    roleList.add(cb.like(root.get("org").get("orgName"),"%"+orgName+"%"));
                }
                return cb.and(roleList.toArray(new Predicate[roleList.size()]));
            }
        },pageable);
        return page;
    }

    @Override
    public Page<User> pageUser(Pageable pageable, MultiValueMap<String, String> parameters){
        Page<User> page = userDao.findPage((Specification<User>) (root, query, cb) -> {
            List<Predicate> roleList = Lists.newArrayList();
            String name = parameters.getFirst("name");
            if(!StringUtils.isEmpty(name)){
                roleList.add(cb.like(root.get("name").as(String.class), "%"+name+"%"));
            }
            String orgName = parameters.getFirst("orgName");
            if (!StringUtils.isEmpty(name)){
                roleList.add(cb.like(root.get("org").get("orgName"),"%"+orgName+"%"));
            }
            return cb.and(roleList.toArray(new Predicate[roleList.size()]));
        },pageable);
        return page;
    }

    @Override
    public Page<User> pageByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters) {
        QUser qUser = QUser.user;
        com.querydsl.core.types.Predicate predicate = qUser.id.longValue().lt(10L).and(qUser.name.like("%超级%"));
        return userDao.findAll(predicate,pageable);
    }

    @Override
    public List<Tuple> pageTupleByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters) {
        JPAQuery jpaQuery  = userDao.getJpaQuery();
        QUser qUser = QUser.user ;
        QRole qRole = QRole.role;
        com.querydsl.core.types.Predicate predicate = qUser.roleId.longValue().eq(qRole.id.longValue()).and(qUser.name.like("%"));
        jpaQuery.select(QUser.user, QRole.role)
                    .from(QUser.user, QRole.role)
                    .where(predicate);
        return jpaQuery.fetch();
    }

    @Override
    public List<DropDownVo> findByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters) {
        JPAQuery jpaQuery  = userDao.getJpaQuery();
        QUser qUser = QUser.user ;
        QRole qRole = QRole.role;
        com.querydsl.core.types.Predicate predicate = qUser.roleId.longValue().eq(qRole.id.longValue()).and(qUser.name.like("%"));
        jpaQuery.select(Projections.bean(DropDownVo.class,
                            qUser.id.as("id"),
                            qUser.name.as("text"),//使用别名对应DropDownVo内的text
                            qRole.name.as("value")))
                .from(QUser.user, QRole.role)
                .where(predicate)
                .orderBy(qUser.id.desc());
        return jpaQuery.fetch();
    }

    @Override
    public QueryResults<Tuple> pageTupleLeftJoinByQueryDsl(Pageable pageable, MultiValueMap<String, String> parameters) {
        JPAQuery jpaQuery  = userDao.getJpaQuery();
        com.querydsl.core.types.Predicate predicate = QUser.user.name.like("%").and(QRole.role.name.like("%坐zuod%"));
        jpaQuery.select(QUser.user, QRole.role, QOrg.org)
                .from(QUser.user)
                .leftJoin(QRole.role)
                .on(QUser.user.roleId.longValue().eq(QRole.role.id.longValue()))
                .leftJoin(QOrg.org)
                .on(QUser.user.org.id.eq(QOrg.org.id))
                .where(predicate)
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize());

        return jpaQuery.fetchResults();
    }


}

package xishu.web;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.errors.StopWalkException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.revwalk.filter.RevFilter;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @ClassName JGitTest
 * @Description: TODO
 * @Author xuxigang
 * @Date 2023/8/19
 * @Version
 **/
public class JGitTest {
    private String gitPath = "D:\\workspaces\\gitee\\train-springboot";
    File file = new File(gitPath);
    Git git;
    Repository repository;

    @Before
    public void init(){
        try {
            git = Git.open(file);
            repository = git.getRepository();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test() throws Exception{
        System.out.println("11");
        LogCommand logCommand = git.log();
        RevFilter revFilter = new RevFilter() {
            @Override
            public boolean include(RevWalk revWalk, RevCommit revCommit) throws StopWalkException, MissingObjectException, IncorrectObjectTypeException, IOException {
//                revCommit.getCommitTime().
                return false;
            }

            @Override
            public RevFilter clone() {
                return null;
            }
        };
//        logCommand.setRevFilter()
        Iterable<RevCommit> call = logCommand.setMaxCount(5).call();
        for (RevCommit revCommit : call) {
            System.out.print(revCommit.getFullMessage() + " ");
            System.out.println(revCommit.getAuthorIdent().getName());
            System.out.println(new Date(revCommit.getCommitTime()).toString());
        }

    }
}

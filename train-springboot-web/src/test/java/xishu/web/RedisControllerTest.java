package xishu.web;

import com.alibaba.fastjson.JSON;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import xishu.core.form.DropDownVo;
import xishu.web.dao.UserDao;
import xishu.web.entity.Org;
import xishu.web.entity.QRole;
import xishu.web.entity.QUser;
import xishu.web.entity.User;
import xishu.web.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by xuxigang on 2018/7/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XishuApplication.class)
public class RedisControllerTest extends MvcTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void testSds() {
        String name = redisTemplate.opsForValue().get("user:name");
        System.out.println(name);

//        redisTemplate.expire("user:name",10, TimeUnit.SECONDS);

        redisTemplate.opsForHash().put("user:roles","role:1", "admin");

        redisTemplate.convertAndSend("chan1","hello22...");
    }

}

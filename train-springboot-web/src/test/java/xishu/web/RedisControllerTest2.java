package xishu.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by xuxigang on 2018/7/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XishuApplication.class)
public class RedisControllerTest2 extends MvcTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void testSub() {
        RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
        connection.subscribe(new MessageListener() {
            @Override
            public void onMessage(Message message, byte[] bytes) {
                byte [] bodyBytes = message.getBody();
                System.out.println(bodyBytes.toString());
//                System.out.println(new String(bodyBtyes));
                System.out.println(bytes.toString());
//                System.out.println(new String(bytes));

            }
        }, "chan1".getBytes());
    }

}

package xishu.web;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import xishu.web.controller.role.RoleQuery;
import xishu.web.dao.RoleDao;

/**
 * Created by xuxigang on 2018/7/26.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XishuApplication.class)
public class RoleControllerTest extends MvcTest {

    @Autowired
    private RoleDao roleDao;


    @Test
    public void testQuery() throws Exception{
        RoleQuery roleQuery = new RoleQuery();
        MvcResult mvcResult = mockMvc
                .perform(// 1
                        MockMvcRequestBuilders.get("/role/query")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(JSON.toJSONString(roleQuery))
                )
                .andReturn();// 4

        int status = mvcResult.getResponse().getStatus(); // 5
        System.out.println(status);
        String responseString = mvcResult.getResponse().getContentAsString(); // 6
        System.out.println(responseString);
    }



    @Test
    public void testRoleTest() throws Exception {
        MvcResult mvcResult = mockMvc
                .perform(// 1
                        MockMvcRequestBuilders.get("/role/test")
                )
                .andReturn();// 4

        int status = mvcResult.getResponse().getStatus(); // 5
        System.out.println(status);
        String responseString = mvcResult.getResponse().getContentAsString(); // 6
        System.out.println(responseString);
    }
}

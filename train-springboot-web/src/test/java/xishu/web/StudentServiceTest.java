package xishu.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xishu.web.entity.Student;
import xishu.web.service.student.IStudentService;

import java.util.List;

/**
 * @author xuxigang
 * @created 2019-08-22
 */
@SpringBootTest(classes = XishuApplication.class)
@RunWith(SpringRunner.class)
public class StudentServiceTest {

    @Autowired
    private IStudentService studentService;

    @Test
    public void testFindAllStudents(){
        List<Student> students = studentService.findAll();
        System.out.println("before Add :" + students.size());
//        assertEquals(students.size(),0);
        Student student = new Student();
        student.setName("xuxigang");
        try {
            studentService.mockExceptionAdd(student);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        students = studentService.findAll();
        System.out.println("After Add :" + students.size());
    }
}

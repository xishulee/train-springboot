package xishu.web;

import com.alibaba.fastjson.JSON;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import xishu.core.form.DropDownVo;
import xishu.web.dao.UserDao;
import xishu.web.entity.Org;
import xishu.web.entity.QRole;
import xishu.web.entity.QUser;
import xishu.web.entity.User;
import xishu.web.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by xuxigang on 2018/7/29.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = XishuApplication.class)
public class UserControllerTest extends MvcTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private IUserService userService;

    @Test
    public void testFindUsers() {
        List<User> users = userDao.findAll();
        users.forEach(user -> {
            System.out.println(user.getName());
            System.out.println(user.getOrg().getOrgName());
            System.out.println("++++++++++++++++++");
        });
    }

    @Test
    public void testFindUsersByNativeSql() {
        List<User> users = userDao.findUsers();
        users.forEach(user -> {
            System.out.println(user.getName());
            System.out.println(user.getOrg().getOrgName());
            System.out.println("=====================");
        });

        List<Object[]> lists = userDao.findUsersBySql();
        lists.forEach((o) -> {
            Object[] obj = (Object[]) o;
            for (int i = 0; i < obj.length; i++) {
                System.out.println(obj[i]);
            }
            System.out.println("++++++++++++++++++");
        });
    }

    @Test
    public void testDropDownVo() {
        List<DropDownVo> vos = userDao.findDropDown();
        vos.forEach(vo -> {
            System.out.println(vo.getId());
            System.out.println(vo.getText());
            System.out.println(vo.getValue());
            System.out.println("*********************");
        });

    }

    @Test
    public void testFindByParams() {
        List<Map> users = userDao.findByParams("select user.id,user.code,user.name from User user");
        System.out.println(JSON.toJSON(users));
       /* users.forEach(map -> {
            map.forEach((key, value) -> {
                System.out.println(key);
                System.out.println(value);
                System.out.println("++++++++++++++++++");
            });
        });*/
    }

    @Test
    public void testFindYihua() {
        List<Map<String,Object>> users = userDao.findYihua();
        System.out.println(JSON.toJSON(users));
        users.forEach(map -> {
            map.forEach((key, value) -> {
//                System.out.println(key);
                System.out.println(key + "=" + value);
            });
            System.out.println("++++++++++++++++++");
        });
    }

    @Test
    public void testFind() {
        List<Object[]> users = userDao.findUser();
        users.forEach(o -> {
            Object[] obj = (Object[]) o;
            User user = (User)obj[0];
            Org org = (Org)obj[1];
            System.out.println(user.getName());
            System.out.println(org.getOrgName());
            System.out.println("&&&&&&&&&&&&&&&&&&&&&&&");
        });
    }

    @Test
//    @Ignore
    public void testFindBySql(){
        List<DropDownVo> lists = userService.findBySql();
        lists.forEach(vo ->{
            System.out.println(vo.getId());
            System.out.println(vo.getText());
            System.out.println(vo.getValue());
            System.out.println("%%%%%%%");
        });

       lists = userService.findBySql1();
       lists.forEach(vo ->{
            System.out.println(vo.getId());
            System.out.println(vo.getText());
            System.out.println(vo.getValue());
            System.out.println("~~~~~~~~~~~~~");
        });
    }

    @Test
    public void testFindListMapBySql(){
       List<Map> lists =  userService.findListMapBySql();
       System.out.println(JSON.toJSON(lists));
       lists.forEach(map -> {
           System.out.println(JSON.toJSON(map));
//           map.forEach((key,value) ->{
//               System.out.println(key);
//               System.out.println(value);
//               System.out.println("++++++++++++++++++");
//           });
       });
    }

    /**
     * 实体有关联关系的查询
     */
    @Test
    public void testFindAllPageBySpecification(){
        Pageable pageable = PageRequest.of(0,2);
        MultiValueMap<String,String> filters = new LinkedMultiValueMap<>();
        filters.put("name", Arrays.asList("超级"));
        filters.put("orgName",Arrays.asList("总"));
        Page<User> page  =  userService.page(pageable,filters);
        System.out.println(page.getContent().get(0).getName());
    }

    @Test
    public void testFindAllPageByQueryDsl(){
        Pageable pageable = PageRequest.of(0,2);
        MultiValueMap<String,String> filters = new LinkedMultiValueMap<>();
        filters.put("name", Arrays.asList("超级"));
        filters.put("orgName",Arrays.asList("总"));
        Page<User> page  =  userService.pageByQueryDsl(pageable,filters);
        System.out.println(page.getContent().get(0).getName());
    }

    /**
     * 产生2条sql语句，
     * select user0_.id as id1_2_0_, role1_.id as id1_1_1_, user0_.access_time as access_t2_2_0_, user0_.code as code3_2_0_, user0_.login_fail_count as login_fa4_2_0_, user0_.enabled as enabled5_2_0_, user0_.freeze_time as freeze_t6_2_0_, user0_.name as name7_2_0_, user0_.org_id as org_id12_2_0_, user0_.pwd as pwd8_2_0_, user0_.phone as phone9_2_0_, user0_.role_id as role_id10_2_0_, user0_.state as state11_2_0_,
     *        role1_.code as code2_1_1_, role1_.is_valid as is_valid3_1_1_, role1_.name as name4_1_1_, role1_.note as note5_1_1_
     *  from sm_user user0_ cross join sm_role role1_
     *  where user0_.role_id=role1_.id and (user0_.name like ? escape '!')
     *
     * 虽然没有调用user.getOrg()但是还是执行了一次org的查找
     * select org0_.id as id1_0_0_, org0_.org_address as org_addr2_0_0_, org0_.org_code as org_code3_0_0_, org0_.org_level as org_leve4_0_0_, org0_.org_name as org_name5_0_0_, org0_.org_type as org_type6_0_0_, org0_.org_zip as org_zip7_0_0_, org0_.parent_id as parent_i8_0_0_
     * from sm_org org0_ where org0_.id=?
     */
    @Test
    public void testFindAllByQueryDsl(){
        Pageable pageable = PageRequest.of(0,2);
        MultiValueMap<String,String> filters = new LinkedMultiValueMap<>();
        filters.put("name", Arrays.asList("超级"));
        filters.put("orgName",Arrays.asList("总"));
        List<Tuple> lists  =  userService.pageTupleByQueryDsl(pageable,filters);
        lists.forEach(tuple -> {
            System.out.println(tuple.get(QUser.user).getName());
            System.out.println(tuple.get(QRole.role).getName());
            System.out.println("=============");
        });
    }

    /***
     *  select user0_.id as id1_2_0_, role1_.id as id1_1_1_, user0_.access_time as access_t2_2_0_, user0_.code as code3_2_0_, user0_.login_fail_count as login_fa4_2_0_, user0_.enabled as enabled5_2_0_, user0_.freeze_time as freeze_t6_2_0_, user0_.name as name7_2_0_, user0_.org_id as org_id12_2_0_, user0_.pwd as pwd8_2_0_, user0_.phone as phone9_2_0_, user0_.role_id as role_id10_2_0_, user0_.state as state11_2_0_,
     *         role1_.code as code2_1_1_, role1_.is_valid as is_valid3_1_1_, role1_.name as name4_1_1_, role1_.note as note5_1_1_
     *  from sm_user user0_ left outer join sm_role role1_ on (user0_.role_id=role1_.id)
     *  where user0_.name like ? escape '!'
     *  机构也写成左关联，就不会有下面这条sql
     *  select org0_.id as id1_0_0_, org0_.org_address as org_addr2_0_0_, org0_.org_code as org_code3_0_0_, org0_.org_level as org_leve4_0_0_, org0_.org_name as org_name5_0_0_, org0_.org_type as org_type6_0_0_, org0_.org_zip as org_zip7_0_0_, org0_.parent_id as parent_i8_0_0_
     *  from sm_org org0_ where org0_.id=?
     *
     *  select count(user0_.id) as col_0_0_ from sm_user user0_ left outer join sm_role role1_ on (user0_.role_id=role1_.id) left outer join sm_org org2_ on (user0_.org_id=org2_.id) where (user0_.name like ? escape '!') and (role1_.name like ? escape '!')
     *  select user0_.id as id1_2_0_, role1_.id as id1_1_1_, org2_.id as id1_0_2_, user0_.access_time as access_t2_2_0_, user0_.code as code3_2_0_, user0_.login_fail_count as login_fa4_2_0_, user0_.enabled as enabled5_2_0_, user0_.freeze_time as freeze_t6_2_0_, user0_.name as name7_2_0_, user0_.org_id as org_id12_2_0_, user0_.pwd as pwd8_2_0_, user0_.phone as phone9_2_0_, user0_.role_id as role_id10_2_0_, user0_.state as state11_2_0_, role1_.code as code2_1_1_, role1_.is_valid as is_valid3_1_1_, role1_.name as name4_1_1_, role1_.note as note5_1_1_, org2_.org_address as org_addr2_0_2_, org2_.org_code as org_code3_0_2_, org2_.org_level as org_leve4_0_2_, org2_.org_name as org_name5_0_2_, org2_.org_type as org_type6_0_2_, org2_.org_zip as org_zip7_0_2_, org2_.parent_id as parent_i8_0_2_ from sm_user user0_ left outer join sm_role role1_ on (user0_.role_id=role1_.id) left outer join sm_org org2_ on (user0_.org_id=org2_.id) where (user0_.name like ? escape '!') and (role1_.name like ? escape '!') limit ?
     *
     */
    @Test
    public void testFindAllLeftJoinByQueryDsl(){
        Pageable pageable = PageRequest.of(0,2);
        MultiValueMap<String,String> filters = new LinkedMultiValueMap<>();
        filters.put("name", Arrays.asList("超dd级"));
        filters.put("orgName",Arrays.asList("总zongzo"));
        QueryResults<Tuple> results  =  userService.pageTupleLeftJoinByQueryDsl(pageable,filters);
        System.out.println(results.getLimit());
        System.out.println(results.getOffset());
        System.out.println(results.getTotal());
        results.getResults().forEach(tuple -> {
            System.out.println(tuple.get(QUser.user).getName());
            System.out.println(tuple.get(QRole.role).getName());
            System.out.println("=============");
        });

        System.out.println(JSON.toJSONString(results));

    }

    @Test
    public void testFindByQueryDsl(){
        Pageable pageable = PageRequest.of(0,2);
        MultiValueMap<String,String> filters = new LinkedMultiValueMap<>();
        filters.put("name", Arrays.asList("超dd级"));
        filters.put("orgName",Arrays.asList("总zongzo"));
        List<DropDownVo> results  =  userService.findByQueryDsl(pageable,filters);
        results.forEach(vo -> {
            System.out.println(vo.getId());
            System.out.println(vo.getText());
            System.out.println(vo.getValue());
            System.out.println("=============");
        });

        System.out.println(JSON.toJSONString(results));

    }
}

-- ******************************************************
-- 表名：SM_USER(用户信息表)
-- ****************************************************** 
INSERT INTO SM_USER(CODE,NAME,PWD,ENABLED,STATE,ROLE_ID,ORG_ID) VALUES ('admin', '超级用户', '21232f297a57a5a743894a0e4a801fc3', 'N',0,1,1);
INSERT INTO SM_USER(CODE,NAME,PWD,ENABLED,STATE,ROLE_ID,ORG_ID) VALUES ('t001', '坐席人员', '21232f297a57a5a743894a0e4a801fc3', 'N',0,2,1);

-- ******************************************************
-- 表名：SM_PERMISSION(菜单信息表)
-- ****************************************************** 
INSERT INTO SM_PERMISSION VALUES ('A', null, 'system', '系统管理', '0', '0', '0', null, 'fa-cogs', null, null);
INSERT INTO SM_PERMISSION VALUES ('A01', null, 'system-user', '用户管理', 'A', '0', '1', null, 'fa-user', null, null);
INSERT INTO SM_PERMISSION VALUES ('A02', null, 'system-teller', '坐席管理', 'A', '0', '1', null, 'fa-tablet', null, null);
INSERT INTO SM_PERMISSION VALUES ('A03', null, 'system-role', '角色管理', 'A', '0', '1', null, 'fa-group', null, null);
INSERT INTO SM_PERMISSION VALUES ('A04', null, 'system-org', '机构管理', 'A', '0', '1', null, 'fa-sitemap', null, null);
INSERT INTO SM_PERMISSION VALUES ('A05', null, 'system-device', '设备管理', 'A', '0', '1', null, 'fa-wrench', null, null);
INSERT INTO SM_PERMISSION VALUES ('A06', null, 'system-parameter', '系统参数', 'A', '0', '1', null, 'fa-gear', null, null);
INSERT INTO SM_PERMISSION VALUES ('A07', null, 'system-logs', '日志管理', 'A','0','1',null,'fa-book',null,null);

INSERT INTO SM_PERMISSION VALUES ('B', 	 null, 'business', '业务管理', '0', '0', '0', null, 'fa-list-alt', null, null);
INSERT INTO SM_PERMISSION VALUES ('B01', null, 'business-transtype', '业务类型', 'B', '0', '1', null, 'fa-th', null, null);
INSERT INTO SM_PERMISSION VALUES ('B02', null, 'business-room', '通道管理', 'B', '0', '1', null, 'fa-road', null, null);
INSERT INTO SM_PERMISSION VALUES ('B03', null, 'business-customer', '客户信息', 'B', '0', '1', null, 'fa-info', null, null);
INSERT INTO SM_PERMISSION VALUES ('B04', null, 'business-telleraudit', '审核记录', 'B', '0', '1', null, 'fa-legal', null, null);


-- ******************************************************
-- 表名：SM_ROLE(角色信息表)
-- ****************************************************** 
INSERT INTO SM_ROLE VALUES ('1','超级管理员','001','有效','');
INSERT INTO SM_ROLE VALUES ('2','坐席','002','有效','');

-- ******************************************************
-- 表名：SM_ROLE_PERMISSION(角色权限表)
-- ****************************************************** 
INSERT INTO SM_ROLE_PERMISSION VALUES ('1','A','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('2','A01','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('3','A02','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('4','A03','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('5','A04','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('6','A05','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('7','A06','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('8','A07','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('9','B','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('10','B01','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('11','B02','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('12','B03','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('13','B04','1');
INSERT INTO SM_ROLE_PERMISSION VALUES ('14','A','2');
INSERT INTO SM_ROLE_PERMISSION VALUES ('15','A06','2');
INSERT INTO SM_ROLE_PERMISSION VALUES ('16','B','2');
INSERT INTO SM_ROLE_PERMISSION VALUES ('17','B03','2');
INSERT INTO SM_ROLE_PERMISSION VALUES ('18','B04','2');


-- ******************************************************
-- 动作：数据添加
-- 表名：SM_ORG(机构信息表)
-- 日期：20170602
-- ****************************************************** 
INSERT INTO SM_ORG VALUES ('1', '1', 'js001', '总行', '0', '0', '1', '0');


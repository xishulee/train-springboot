package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @ClassName ReactorController
 * @Description: TODO
 * @Author xuxigang
 * @Date 2021/4/8
 * @Version
 **/
@RestController
@RequestMapping("/test")
public class ReactorController {

    /**
     * 接口的返回类型，对于webflux 来讲接口的返回参数只有两种 Flux<T>或者是 Mono<T>
     *     Flux 返回0个或多个结果，可能是无限个
     *     Mono: 返回0个或1个结果
     *     是Publisher的一种形式
     * @return
     */
    @GetMapping("/hello")
    public Mono<String> helloWorld(){
        return Mono.just("hello world");
    }

}

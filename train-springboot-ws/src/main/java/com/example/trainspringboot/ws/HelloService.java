package com.example.trainspringboot.ws;

import javax.jws.WebService;

/**
 * @ClassName HelloService
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/10/29
 * @Version
 **/
public interface HelloService {
    int add(int a, int b);
}

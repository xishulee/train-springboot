package com.example.trainspringboot.ws;

import javax.jws.WebService;

/**
 * @ClassName HelloServiceImpl
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/10/29
 * @Version
 **/
@WebService
public class HelloServiceImpl implements HelloService{
    @Override
    public int add(int a, int b) {
        return a + b;
    }
}

package com.example.trainspringboot.ws;

import javax.xml.ws.Endpoint;

/**
 * @ClassName Main
 * @Description: TODO
 * @Author xuxigang
 * @Date 2022/10/29
 * @Version
 **/
public class Main {
    public static void main(String[] args) {

        String url = "http://localhost:9008/hello";
        Endpoint.publish(url, new HelloServiceImpl());
        System.out.println("发布ws成功");
    }
}

package com.example.trainspringboot.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainSpringbootWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TrainSpringbootWsApplication.class, args);
    }

}

package com.xishulee.xishu.admin;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class XishuAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(XishuAdminApplication.class, args);
    }

}

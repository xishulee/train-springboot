package com.xishulee.xishu.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @ClassName DemoEnv
 * @Description: TODO
 * @Author xuxigang
 * @Date 2024/3/7
 * @Version
 **/
@Component
public class DemoEnv implements EnvironmentPostProcessor, Ordered {

    private static final Integer POST_PROCESSOR_ORDER = Integer.MIN_VALUE+10;

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        MutablePropertySources propertySources = environment.getPropertySources();
        for (PropertySource<?> propertySource : propertySources) {
            System.out.printf("name: " +
                    "", propertySource.getName());
        }
    }

    @Override
    public int getOrder() {
        return DemoEnv.POST_PROCESSOR_ORDER;
    }
}

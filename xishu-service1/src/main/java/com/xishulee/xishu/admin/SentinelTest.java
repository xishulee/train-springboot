package com.xishulee.xishu.admin;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName SentinelTest
 * @Description: TODO
 * @Author xuxigang
 * @Date 2020/12/8
 * @Version
 **/
public class SentinelTest {
    public static void main(String[] args) {
        initRule();
        while(true){
//            hello();
            try(Entry entry = SphU.entry("HelloWorld")) {
                System.out.println("hello world!");
            } catch (BlockException e) {
                System.out.println("blocked.");
            }
        }
    }

    private static void initRule() {
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("HelloWorld");
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        // Set limit QPS to 20.
        rule.setCount(20);
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
    }

    @SentinelResource("HelloWorld")
    private static void hello(){
        System.out.println("hello world!");
    }
}

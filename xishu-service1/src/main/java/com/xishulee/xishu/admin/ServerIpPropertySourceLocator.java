package com.xishulee.xishu.admin;

import org.springframework.cloud.bootstrap.config.PropertySourceLocator;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @ClassName ServerIpPropertySourceLocator
 * @Description: TODO
 * @Author xuxigang
 * @Date 2024/3/7
 * @Version
 **/
@Component
public class ServerIpPropertySourceLocator implements PropertySourceLocator {

    @Override
    public PropertySource<?> locate(Environment environment) {
        HashMap<String, Object> source = new HashMap<>();
        source.put("serverIp", "1111111");
        return new MapPropertySource("serverIpPropertySource", source);
    }
}

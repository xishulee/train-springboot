package com.xishulee.xishu.admin;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class XishuService1Application {

    public String getServerIp() {
        return serverIp;
    }

    @Value("${serverIp}")
    private String serverIp;

    public static void main(String[] args) {
//        XishuService1Application a = new XishuService1Application();
        ConfigurableApplicationContext run = SpringApplication.run(XishuService1Application.class, args);
//        System.out.println("this.serverIp: " + a.getServerIp());
    }

}

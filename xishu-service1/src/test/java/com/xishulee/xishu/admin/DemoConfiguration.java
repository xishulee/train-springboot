package com.xishulee.xishu.admin;

import org.springframework.boot.env.PropertySourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName DemoConfiguration
 * @Description: TODO
 * @Author xuxigang
 * @Date 2024/3/7
 * @Version
 **/
@Configuration
public class DemoConfiguration {

    @Bean
    public PropertySourceLoader serverIpPropertySourceLoader(){
        return null;
    }
}
